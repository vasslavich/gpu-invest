﻿
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <chrono>
#include <vector>
#include <stdio.h>
#include <stdexcept>
#include <iostream>
#include "./params.h"

/**
 * Matrix multiplication (CUDA Kernel) on the device: C = A * B
 * wA is A's width and wB is B's width
 */
__global__ void MatrixMulCUDA(
    size_t m,
    size_t k,
    size_t n,
    double* C,
    const double* A,
    const double* B){

    // Block index
    int bx = blockIdx.x;
    int by = blockIdx.y;

    // Thread index
    int tx = threadIdx.x;
    int ty = threadIdx.y;

    // Index of the first sub-matrix of A processed by the block
    int aBegin = k * BLOCK_SIZE * by;

    // Index of the last sub-matrix of A processed by the block
    int aEnd = aBegin + k - 1;

    // Step size used to iterate through the sub-matrices of A
    int aStep = BLOCK_SIZE;

    // Index of the first sub-matrix of B processed by the block
    int bBegin = BLOCK_SIZE * bx;

    // Step size used to iterate through the sub-matrices of B
    int bStep = BLOCK_SIZE * n;

    // Csub is used to store the element of the block sub-matrix
    // that is computed by the thread
    double Csub = 0;

    // Loop over all the sub-matrices of A and B
    // required to compute the block sub-matrix
    for(int a = aBegin, b = bBegin;
        a <= aEnd;
        a += aStep, b += bStep){
        // Declaration of the shared memory array As used to
        // store the sub-matrix of A
        __shared__ double As[BLOCK_SIZE][BLOCK_SIZE];

        // Declaration of the shared memory array Bs used to
        // store the sub-matrix of B
        __shared__ double Bs[BLOCK_SIZE][BLOCK_SIZE];

        // Load the matrices from device memory
        // to shared memory; each thread loads
        // one element of each matrix
        As[ty][tx] = A[a + k * ty + tx];
        Bs[ty][tx] = B[b + n * ty + tx];

        // Synchronize to make sure the matrices are loaded
        __syncthreads();

        // Multiply the two matrices together;
        // each thread computes one element
        // of the block sub-matrix
#pragma unroll

        for(int k = 0; k < BLOCK_SIZE; ++k){
            Csub += As[ty][k] * Bs[k][tx];
            }

        // Synchronize to make sure that the preceding
        // computation is done before loading two new
        // sub-matrices of A and B in the next iteration
        __syncthreads();
        }

    // Write the block sub-matrix to device memory;
    // each thread writes one element
    int c = n * BLOCK_SIZE * by + BLOCK_SIZE * bx;
    C[c + n * ty + tx] = Csub;
    }

void invoke_kernel(kernel_params * args){
    int block_size = args->block_size;
    const double* a = args->a;
    const double* b = args->b; 
    double* c = args->c;

    size_t m = args->m;
    size_t k = args->k;
    size_t n = args->n;

    dim3 threads(block_size, block_size);
    dim3 grid(k / threads.x, m / threads.y);

    // warm up
    MatrixMulCUDA<<<grid, threads, 0, 0>>> (m, k, n, c, a, b);
    }

