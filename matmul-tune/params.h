#pragma once

#include <cstdint>

struct kernel_params{
	size_t m{};
	size_t k{};
	size_t n{};

	double* a;
	double* b;
	double* c;

	size_t block_size;
	};


