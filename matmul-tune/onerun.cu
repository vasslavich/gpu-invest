#include <cstdint>
#include <iostream>
#include <vector>
#include <filesystem>
#include <regex>
#include <chrono>
#include "cuda_runtime.h"
#include "cuda_runtime_api.h"


struct kernel_params{
	size_t m{};
	size_t k{};
	size_t n{};

	double* a{};
	double* b{};
	double* c{};
	};
	
void cuda_status_check(cudaError_t err){
    if(cudaSuccess != err){
        const char* errs = cudaGetErrorString(err);
        throw std::runtime_error{errs};
        }
    }

#define S(x) #x
#define CUDA_CALL_CHECK(call){cuda_status_check((call));}

//#define BLOCK_SIZE 16



/******************************************************************************/
// op<trans>( x ) returns x or conj(x).
template< const int conjugate, typename T >
__host__ __device__ static inline
T op( T& x )
{
	    if (conjugate == 1) {
		            return conj(x);
			        } else {
					        return x;
						    }
}


/******************************************************************************/
template<typename T, const int DIM_X, const int DIM_Y, const int BLK_M, const int BLK_N, const int BLK_K, 
	         const int DIM_XA, const int DIM_YA, const int DIM_XB, const int DIM_YB, 
		          const int THR_M, const int THR_N, const int CONJA, const int CONJB>
			  static __device__ 
			  void gemm_template_device_nn(
					      int M, int N, int K,
					          const T* __restrict__ A, int LDA,
						      const T* __restrict__ B, int LDB,
						          T*       __restrict__ C, int LDC,
							      T alpha, T beta )
{
#if (__CUDA_ARCH__ >= 200)
	    int idx = threadIdx.x;  // thread's m dimension
	        int idy = threadIdx.y;  // thread's n dimension

		    int idt = DIM_X * idy + idx;    // thread's global number

		        int idxA = idt % DIM_XA;    // idx within A
			    int idyA = idt / DIM_XA;    // idy within A

			        int idxB = idt % DIM_XB;    // idx within B
				    int idyB = idt / DIM_XB;    // idy within B

				        int blx = blockIdx.x;   // block's m dimension
					    int bly = blockIdx.y;   // block's n dimension

					        __shared__ T sA[BLK_K][BLK_M+1];      // +1 only required if A is transposed
						    __shared__ T sB[BLK_N][BLK_K+1];      // +1 always required

						        // Registers for the innermost loop
						        T rC[THR_N][THR_M];
							    T rA[THR_M];
							        T rB[THR_N];

								    T ra[BLK_K/DIM_YA][BLK_M/DIM_XA];
								        T rb[BLK_N/DIM_YB][BLK_K/DIM_XB];
									    
									    const T *offs_dA = A + blx*BLK_M     + idyA*LDA + idxA;
									        ptrdiff_t boundA = (LDA*(K-1) + M) - ( blx*BLK_M  + idyA*LDA + idxA ) -1;
										        
										    const T *offs_dB = B + bly*BLK_N*LDB + idyB*LDB + idxB;
										        ptrdiff_t boundB = (LDB*(N-1) + K) - ( bly*BLK_N*LDB + idyB*LDB + idxB ) -1;
											    
											    int m, n, k, kk;


											        // Zero C
											        #pragma unroll
											        for (n = 0; n < THR_N; n++)
													        #pragma unroll
													        for (m = 0; m < THR_M; m++)
															            rC[n][m] = make_FloatingPoint(0.0, 0.0);

												    #pragma unroll
												    for (n = 0; n < BLK_K; n += DIM_YA)
													            #pragma unroll
													            for (m = 0; m < BLK_M; m += DIM_XA)
															                sA[n+idyA][m+idxA] = fetch(A, m, n, boundA);
												        
												        #pragma unroll
												        for (n = 0; n < BLK_N; n += DIM_YB)
														        #pragma unroll
														        for (m = 0; m < BLK_K; m += DIM_XB)
																            sB[n+idyB][m+idxB] = fetch(B, m, n, boundB);
													    
													    __syncthreads();

													        for (kk = 0; kk < K-BLK_K; kk += BLK_K)
															    {
																            #ifdef TEXTURE_1D
																                coord_A += BLK_K*LDA;
																		            coord_B += BLK_K;
																			            #else
																			                offs_dA += BLK_K*LDA;
																					            boundA  -= BLK_K*LDA;

																						                offs_dB += BLK_K;
																								            boundB  -= BLK_K;
																									            #endif

																									            #pragma unroll
																									            for (n = 0; n < BLK_K/DIM_YA; n++)
																											                #pragma unroll
																											                for (m = 0; m < BLK_M/DIM_XA; m++)
																														                ra[n][m] = fetch(A, m*DIM_XA, n*DIM_YA, boundA);

																										            #pragma unroll
																										            for (n = 0; n < BLK_N/DIM_YB; n++)
																												                #pragma unroll
																												                for (m = 0; m < BLK_K/DIM_XB; m++)
																															                rb[n][m] = fetch(B, m*DIM_XB, n*DIM_YB, boundB);
																											            
																											            // Multiply
																											            #pragma unroll
																											            for (k = 0; k < BLK_K; k++)
																													            {
																															                // Load A shmem->regs
																															                #pragma unroll
																															                for (m = 0; m < THR_M; m++)
																																		                rA[m] = sA[k][m*DIM_X+idx];

																																	            // Load B shmem->regs
																																	            #pragma unroll
																																	            for (n = 0; n < THR_N; n++)
																																			                    rB[n] = sB[n*DIM_Y+idy][k];

																																		                // Compute
																																		                #pragma unroll
																																		                for (n = 0; n < THR_N; n++) {
																																					                #pragma unroll
																																					                for (m = 0; m < THR_M; m++) {
																																								                    fma(op<CONJA>(rA[m]), op<CONJB>(rB[n]), rC[n][m]);
																																										                    }
																																							            }
																																				        }

																												            __syncthreads();

																													            #pragma unroll
																													            for (n = 0; n < BLK_K/DIM_YA; n++)
																															                #pragma unroll
																															                for (m = 0; m < BLK_M/DIM_XA; m++)
																																		                sA[n*DIM_YA+idyA][m*DIM_XA+idxA] = ra[n][m];
																														            
																														            #pragma unroll
																														            for (n = 0; n < BLK_N/DIM_YB; n++)
																																                #pragma unroll
																																                for (m = 0; m < BLK_K/DIM_XB; m++)
																																			                sB[n*DIM_YB+idyB][m*DIM_XB+idxB] = rb[n][m];
																															            
																															            __syncthreads();
																																        }

														    // Multiply last full (BLK_K) or partial block of
														    // columns of op(A) and rows of op(B).
														    // It's okay that m,n exceed matrix bounds as all work is in registers
														    // or shared memory, and out-of-bounds rC[n][m] will not be saved later.
														    kk = K - kk;
														        #pragma unroll
														        for (k = 0; k < kk; k++)
																    {
																	            // Load A shmem->regs
																	            #pragma unroll
																	            for (m = 0; m < THR_M; m++)
																			                rA[m] = sA[k][m*DIM_X+idx];

																		            // Load B shmem->regs
																		            #pragma unroll
																		            for (n = 0; n < THR_N; n++)
																				                rB[n] = sB[n*DIM_Y+idy][k];

																			            // Compute
																			            #pragma unroll
																			            for (n = 0; n < THR_N; n++) {
																					                #pragma unroll
																					                for (m = 0; m < THR_M; m++) {
																								                fma(op<CONJA>(rA[m]), op<CONJB>(rB[n]), rC[n][m]);
																										            }
																							        }
																				        }

															    // Store C regs->dev
															    if( beta == make_FloatingPoint(0.0,0.0) ) {
																            #pragma unroll
																            for (n = 0; n < THR_N; n++) {
																		                int coord_dCn = bly*BLK_N + n*DIM_Y + idy;
																				            #pragma unroll
																				            for (m = 0; m < THR_M; m++) {
																						                    int coord_dCm = blx*BLK_M + m*DIM_X + idx;
																								                    if (coord_dCm < M && coord_dCn < N) {
																											                        int offsC = coord_dCn*LDC + coord_dCm;

																														                    T &regC = rC[n][m];
																																                        T &memC = C[offsC];

																																			                    memC = mul(alpha, regC);
																																					                    }
																										                }
																					            }
																	        } else {
																			        #pragma unroll
																			        for (n = 0; n < THR_N; n++) {
																					            int coord_dCn = bly*BLK_N + n*DIM_Y + idy;
																						                #pragma unroll
																						                for (m = 0; m < THR_M; m++) {
																									                int coord_dCm = blx*BLK_M + m*DIM_X + idx;
																											                if (coord_dCm < M && coord_dCn < N) {
																														                    int offsC = coord_dCn*LDC + coord_dCm;

																																                        T &regC = rC[n][m];
																																			                    T &memC = C[offsC];

																																					                        memC = add(mul(alpha, regC), mul(beta, memC));
																																								                }
																													            }
																								        }
																				    }
#endif /* (__CUDA_ARCH__ >= 200) */
}


/**
 * Matrix multiplication (CUDA Kernel) on the device: C = A * B
 * wA is A's width and wB is B's width
 */
__global__ void MatrixMulCUDA(
    size_t m,
    size_t k,
    size_t n,
    double* C,
    const double* A,
    const double* B){

    // Block index
    int bx = blockIdx.x;
    int by = blockIdx.y;

    // Thread index
    int tx = threadIdx.x;
    int ty = threadIdx.y;

    // Index of the first sub-matrix of A processed by the block
    int aBegin = k * BLOCK_SIZE * by;

    // Index of the last sub-matrix of A processed by the block
    int aEnd = aBegin + k - 1;

    // Step size used to iterate through the sub-matrices of A
    int aStep = BLOCK_SIZE;

    // Index of the first sub-matrix of B processed by the block
    int bBegin = BLOCK_SIZE * bx;

    // Step size used to iterate through the sub-matrices of B
    int bStep = BLOCK_SIZE * n;

    // Csub is used to store the element of the block sub-matrix
    // that is computed by the thread
    double Csub = 0;

    // Loop over all the sub-matrices of A and B
    // required to compute the block sub-matrix
    for(int a = aBegin, b = bBegin;
        a <= aEnd;
        a += aStep, b += bStep){
        // Declaration of the shared memory array As used to
        // store the sub-matrix of A
        __shared__ double As[BLOCK_SIZE][BLOCK_SIZE];

        // Declaration of the shared memory array Bs used to
        // store the sub-matrix of B
        __shared__ double Bs[BLOCK_SIZE][BLOCK_SIZE];

        // Load the matrices from device memory
        // to shared memory; each thread loads
        // one element of each matrix
        As[ty][tx] = A[a + k * ty + tx];
        Bs[ty][tx] = B[b + n * ty + tx];

        // Synchronize to make sure the matrices are loaded
        __syncthreads();

        // Multiply the two matrices together;
        // each thread computes one element
        // of the block sub-matrix
#pragma unroll

        for(int k = 0; k < BLOCK_SIZE; ++k){
            Csub += As[ty][k] * Bs[k][tx];
            }

        // Synchronize to make sure that the preceding
        // computation is done before loading two new
        // sub-matrices of A and B in the next iteration
        __syncthreads();
        }

    // Write the block sub-matrix to device memory;
    // each thread writes one element
    int c = n * BLOCK_SIZE * by + BLOCK_SIZE * bx;
    C[c + n * ty + tx] = Csub;
    }

void invoke_kernel(kernel_params * args){
    const double* a = args->a;
    const double* b = args->b;
    double* c = args->c;

    size_t m = args->m;
    size_t k = args->k;
    size_t n = args->n;

    dim3 threads(BLOCK_SIZE, BLOCK_SIZE);
    dim3 grid(k / threads.x, m / threads.y);

    // warm up
    MatrixMulCUDA<<<grid, threads, 0, 0>>> (m, k, n, c, a, b);
    }

class Benchmark{
    std::vector<double> h_a;
    std::vector<double> h_b;
    std::vector<double> h_c;

    double* d_a;
    double* d_b;
    double* d_c;

    size_t m;
    size_t k;
    size_t n;

public:
    void make_input(size_t M, size_t K, size_t N){
		std::cout << "make input: " << std::endl;
		std::cout << "    M " << M << std::endl;
		std::cout << "    K " << K << std::endl;
		std::cout << "    N " << N << std::endl;
		
        m = M;
        k = K;
        n = N;

        h_a.resize(m * k, 1.1);
        h_b.resize(k * n, 1.1);
        h_c.resize(m * n, 1.1);
        }

    void copy_to_device(){
		std::cout << "send data to device" << std::endl;
		
        CUDA_CALL_CHECK(cudaMalloc(&d_a, h_a.size() * sizeof(double)));
        CUDA_CALL_CHECK(cudaMalloc(&d_b, h_b.size() * sizeof(double)));
        CUDA_CALL_CHECK(cudaMalloc(&d_c, h_c.size() * sizeof(double)));

        CUDA_CALL_CHECK(cudaMemcpy(d_a, h_a.data(), h_a.size() * sizeof(double),
            cudaMemcpyHostToDevice));

        CUDA_CALL_CHECK(cudaMemcpy(d_b, h_b.data(), h_b.size() * sizeof(double),
            cudaMemcpyHostToDevice));

        CUDA_CALL_CHECK(cudaMemcpy(d_c, h_c.data(), h_c.size() * sizeof(double),
            cudaMemcpyHostToDevice));

        CUDA_CALL_CHECK(cudaDeviceSynchronize());
        CUDA_CALL_CHECK(cudaGetLastError());
        }

    void invoke_kernel(){
		std::cout << "invoke kernel" << std::endl;
		
        kernel_params p;
        p.a = d_a;
        p.b = d_b;
        p.c = d_c;
        p.m = m;
        p.k = k;
        p.n = n;
       
		// warm up
	std::cout << "invoke warm up code" << std::endl;
		::invoke_kernel(std::addressof(p));
		CUDA_CALL_CHECK(cudaDeviceSynchronize());
		
		std::cout << "invoke target test" << std::endl;
        auto startTP = std::chrono::steady_clock::now();

        const int iterCount = 2;
        for(int iter = 0; iter < iterCount; ++iter){
		std::cout << "  iteration " << iter << std::endl;
            ::invoke_kernel(std::addressof(p));
			CUDA_CALL_CHECK(cudaDeviceSynchronize());
            }

        auto stopTP = std::chrono::steady_clock::now();
        auto mcsecTotal = std::chrono::duration_cast<std::chrono::microseconds>(stopTP - startTP);

        // Compute and print the performance
        float mcsecPerMatrixMul = mcsecTotal.count() / iterCount;

        double flopsPerMatrixMul = 2.0 * static_cast<double>(k) *
            static_cast<double>(m) *
            static_cast<double>(n);
        double tFlops =
            flopsPerMatrixMul / (mcsecPerMatrixMul * 1000000.0f);

        std::cout << "Performance= " << tFlops << ",TFlop" << std::endl;
        }

    void copy_from_device(){
        CUDA_CALL_CHECK(cudaDeviceSynchronize());
        CUDA_CALL_CHECK(cudaGetLastError());
        }

    void free_data()noexcept{
        h_a.clear();
        h_b.clear();
        h_c.clear();

        CUDA_CALL_CHECK(cudaFree(d_a));
        CUDA_CALL_CHECK(cudaFree(d_b));
        CUDA_CALL_CHECK(cudaFree(d_c));
        }
    };

/**
 * Program main
 */
int main(int argc, char** argv){

    Benchmark test;
    test.make_input(8192, 8192, 8192);
    test.copy_to_device();
    test.invoke_kernel();
    test.copy_from_device();
    test.free_data();
}
	
