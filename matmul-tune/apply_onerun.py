import subprocess
import time
import math
import sys
import os

warp_size = 32
real_size = 8

# for compute capability 8.0
max_threads_per_block = 1024
max_shmem_per_block = (128*1024)
max_regs_per_thread = 255
max_regs_per_block = 65536

# heuristic constraints
min_threads_per_block = 256

class TuneError(Exception):
	"""Some build error"""
	pass

def execute(cmd):
    print(f'execute: {cmd}')

    start_counter = time.perf_counter_ns()

    retcode = -1
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    while True:
            output = p.stdout.readline()
            if output :
                print(output.strip())
        
            if p.poll() is not None:
                retcode = p.poll()
                break
    
    duration_ns = time.perf_counter_ns() - start_counter
    
    if retcode != 0:
        print("\nSome error")
        raise TuneError
        
    tflops = (2 * pow(8192, 3))/(duration_ns * 1000)
    return tflops
        
def build_args_libraries(llibs):
    string = '';
    for libname in llibs:
        string = string + (' -l' + libname)
    return string
    
def build_args_sources(sources):
    return ' '.join(sources)

def build_args_configuration(block_size):
    return '-DBLOCK_SIZE=' + str(block_size)

def loop_configuration():
    print('loop over configurations')

    min_tflops = 1000000000
    max_tflops = 0
    
    for block_size in range(8,32+1,8):
        shmem_per_block = block_size*block_size*2
        if shmem_per_block > max_shmem_per_block:
            continue
        
        #threads_per_block = bock_size*bock_size
        #if threads_per_block > max_threads_per_block:
        # continue

        #if threads_per_block % warp_size != 0:
        # continue

        #if threads_per_block < min_threads_per_block:
        #  continue
          
        print(f'configure : {block_size}')
        
        compiler = 'nvcc'
        sources_list = ['onerun.cu']
        link_list = ['cudart']

        execute(compiler + ' -o app_gemm ' + build_args_configuration(block_size) + ' '  + build_args_sources(sources_list) + build_args_libraries(link_list))
          
        print(f'execute binary')
        
        tflops = execute('./app_gemm')
        
        if min_tflops > tflops:
            min_tflops = tflops
            
        if max_tflops < tflops:
            max_tflops = tflops
            
        print(f'max TFLOPS={max_tflops}, min TFLOPS={min_tflops}')
        
if __name__ == "__main__":
	loop_configuration()
    
