#include <iostream>
#include <vector>
#include <filesystem>
#include <regex>
#include <chrono>
#include "./params.h"
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <dlfcn.h>
#include <cassert>
#include <string_view>

void cuda_status_check(cudaError_t err){
    if(cudaSuccess != err){
        const char* errs = cudaGetErrorString(err);
        throw std::runtime_error{errs};
        }
    }

#define S(x) #x
#define CUDA_CALL_CHECK(call){cuda_status_check((call));}

class Kernel{
    std::string src_filename;
    std::string param_string;
    std::string func_name;
    std::string scratch_path;
    int indx;

public:
    typedef cudaError_t(*Func)(void*);

    Kernel(std::string src_filename_,
        std::string param_string_,
        std::string func_name_,
        std::string scratch_path_,
        int idx_)
        : src_filename(src_filename_),
        param_string(param_string_),
        func_name(func_name_),
        scratch_path(scratch_path_),
        indx(idx_){

        // kernel.cu -> kernel_1.so, kernel_2.so, etc.
        std::regex pattern("([^\\.]+)\\.([^\\.]+)");
        lib_filename_ = std::regex_replace(src_filename_, pattern, "$1");
        lib_filename_ += "_" + std::to_string(idx_) + ".so";

	std::cout << "lib filename = " << lib_filename_ << std::endl;
	std::cout << "func name = " << func_name << std::endl;
	std::cout << "scratch_path = " << scratch_path << std::endl;
        }

    Func func() const{
        return func_;
        }

    bool compile(bool verbose = false) const{
        std::string cmd_str("nvcc "
            "--compiler-options '-fPIC' "
            "--shared -o ");
        cmd_str += scratch_path_ + lib_filename_ + " ";
        cmd_str += scratch_path_ + src_filename_ + " ";
        cmd_str += param_string_ + " ";

        if(!verbose)
            cmd_str += ">/dev/null 2>&1";
        else
            std::cout << cmd_str << std::endl;

	std::cout << "compile command: " << std::endl;
	std::cout << cmd_str << std::endl;

        int retval = std::system(cmd_str.c_str());
        return (retval == 0 ? true : false);
        } 

    /// \brief
    ///     Opens the dynamic library containing the kernel.
    void open(){
        handle_ = dlopen((scratch_path_ + lib_filename_).c_str(), RTLD_NOW);
        assert(handle_ != nullptr);
        }

    /// \brief
    ///     Loads the kernel's function from the dynamic library.
    void load(){
        func_ = (Func)dlsym(handle_, func_name_.c_str());
        assert(func_ != nullptr);
        }

    /// \brief
    ///     Closes the dynamic library.
    void close() const noexcept{
        int retval = dlclose(handle_);
        if(retval){
            std::cout << "faile close dl handler" << std::endl;
            }
        }

    /// \brief
    ///     Deletes the kernel's dynamic library from the filesystem.
    void remove() const noexcept{
        std::string fullpath(scratch_path_ + lib_filename_);
        int retval = std::remove(fullpath.c_str());
        if(retval){
            std::cout << "failed remove dynamic library" << std::endl;
            }
        }

private:
    const std::string src_filename_; ///< kernel's source filename
    const std::string param_string_; ///< parameters' string for compilation
    const std::string func_name_;    ///< kernel's function name
    const std::string scratch_path_; ///< path to the local scratch directory
    std::string lib_filename_;       ///< kernel's dynamic library filename
    void* handle_;                   ///< kernel's dynamic library handle
    Func func_;                      ///< kernel's function pointer
    int idx_;                        ///< kernel's number
    };

class Benchmark{
    std::vector<double> h_a;
    std::vector<double> h_b;
    std::vector<double> h_c;

    std::string kernel_name{"kernel.cu"};

    double* d_a;
    double* d_b;
    double* d_c;

    size_t m;
    size_t k;
    size_t n;

    std::unique_ptr< Kernel> pKernel;

public:
    void make_input(size_t M, size_t K, size_t N){
        m = M;
        k = K;
        n = N;

        h_a.resize(m * k, 1.1);
        h_b.resize(k * n, 1.1);
        h_c.resize(m * n, 1.1);
        }

    void copy_to_device(){
        CUDA_CALL_CHECK(cudaMalloc(&d_a, h_a.size() * sizeof(double)));
        CUDA_CALL_CHECK(cudaMalloc(&d_b, h_b.size() * sizeof(double)));
        CUDA_CALL_CHECK(cudaMalloc(&d_c, h_c.size() * sizeof(double)));

        CUDA_CALL_CHECK(cudaMemcpy(d_a, h_a.data(), h_a.size() * sizeof(double),
            cudaMemcpyHostToDevice));

        CUDA_CALL_CHECK(cudaMemcpy(d_b, h_b.data(), h_b.size() * sizeof(double),
            cudaMemcpyHostToDevice));

        CUDA_CALL_CHECK(cudaMemcpy(d_c, h_c.data(), h_c.size() * sizeof(double),
            cudaMemcpyHostToDevice));

        CUDA_CALL_CHECK(cudaDeviceSynchronize());
        CUDA_CALL_CHECK(cudaGetLastError());
        }

    void build_kernel(){
        pKernel = std::make_unique<Kernel>(
			std::string{"kernel.cu"},
			std::string{"arch=sm_80"},
			std::string{"invoke_kernel"},
			std::string{},
			0);
        pKernel->compile();
        pKernel->open();
        pKernel->load();
        }

    void invoke_kernel(size_t block_size){
        kernel_params p;
        p.a = d_a;
        p.b = d_b;
        p.c = d_c;
        p.m = m;
        p.k = k;
        p.n = n;
        p.block_size = block_size;
       
        auto startTP = std::chrono::steady_clock::now();

        const int iterCount = 2;
        for(int iter = 0; iter < iterCount; ++iter){
            pKernel->func()(std::addressof(p));
            }

        auto stopTP = std::chrono::steady_clock::now();
        auto mcsecTotal = std::chrono::duration_cast<std::chrono::microseconds>(stopTP - startTP);

        // Compute and print the performance
        float mcsecPerMatrixMul = mcsecTotal.count() / iterCount;

        double flopsPerMatrixMul = 2.0 * static_cast<double>(k) *
            static_cast<double>(m) *
            static_cast<double>(n);
        double tFlops =
            flopsPerMatrixMul / (mcsecPerMatrixMul * 1000000.0f);

        std::cout << "Performance= " << tFlops << ",TFlop" << std::endl;
        }

    void copy_from_device(){
        CUDA_CALL_CHECK(cudaDeviceSynchronize());
        CUDA_CALL_CHECK(cudaGetLastError());
        }

    void free_data()noexcept{
        h_a.clear();
        h_b.clear();
        h_c.clear();

        CUDA_CALL_CHECK(cudaFree(d_a));
        CUDA_CALL_CHECK(cudaFree(d_b));
        CUDA_CALL_CHECK(cudaFree(d_c));
        }
    };

/**
 * Program main
 */
int main(int argc, char** argv){
    size_t block_size = 16;
    for(; block_size < 64; block_size += 16){
        Benchmark test;
        test.make_input(8192, 8192, 8192);
        test.copy_to_device();
        test.build_kernel();
        test.invoke_kernel(block_size);
        test.copy_from_device();
        test.free_data();
        }
    }

