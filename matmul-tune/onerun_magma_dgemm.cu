#pragma once


struct kernel_params{
	size_t m{};
	size_t k{};
	size_t n{};

	double* a{};
	double* b{};
	double* c{};
	
	double alpha{};
	double beta{};
	};
	
/******************************************************************************/
// op<trans>( x ) returns x or conj(x).
template< const int conjugate, typename T >
__host__ __device__ static inline
T op( T& x )
{
    if (conjugate == 1) {
        return conj(x);
    } else {
        return x;
    }
}

#define THR_M = (BLK_M /DIM_X)
#define THR_N = (BLK_N / DIM_Y)
#define CONJA 0
#define CONJB 0

static __device__ 
void gemm_template_device_nn(
    int M, int N, int K,
    const double* __restrict__ A, int LDA,
    const double* __restrict__ B, int LDB,
    double*       __restrict__ C, int LDC,
    double alpha, double beta )
{
#if (__CUDA_ARCH__ >= 200)
    int idx = threadIdx.x;  // thread's m dimension
    int idy = threadIdx.y;  // thread's n dimension

    int idt = DIM_X * idy + idx;    // thread's global number

    int idxA = idt % DIM_XA;    // idx within A
    int idyA = idt / DIM_XA;    // idy within A

    int idxB = idt % DIM_XB;    // idx within B
    int idyB = idt / DIM_XB;    // idy within B

    int blx = blockIdx.x;   // block's m dimension
    int bly = blockIdx.y;   // block's n dimension

    __shared__ double sA[BLK_K][BLK_M+1];      // +1 only required if A is transposed
    __shared__ double sB[BLK_N][BLK_K+1];      // +1 always required

    // Registers for the innermost loop
	double rC[THR_N][THR_M];
    double rA[THR_M];
    double rB[THR_N];

    double ra[BLK_K/DIM_YA][BLK_M/DIM_XA];
    double rb[BLK_N/DIM_YB][BLK_K/DIM_XB];
    
    const double *offs_dA = A + blx*BLK_M     + idyA*LDA + idxA;
    ptrdiff_t boundA = (LDA*(K-1) + M) - ( blx*BLK_M  + idyA*LDA + idxA ) -1;
        
    const double *offs_dB = B + bly*BLK_N*LDB + idyB*LDB + idxB;
    ptrdiff_t boundB = (LDB*(N-1) + K) - ( bly*BLK_N*LDB + idyB*LDB + idxB ) -1;
    
    int m, n, k, kk;


    // Zero C
    #pragma unroll
    for (n = 0; n < THR_N; n++)
        #pragma unroll
        for (m = 0; m < THR_M; m++)
            rC[n][m] = make_FloatingPoint(0.0, 0.0);

    #pragma unroll
    for (n = 0; n < BLK_K; n += DIM_YA)
        #pragma unroll
        for (m = 0; m < BLK_M; m += DIM_XA)
            sA[n+idyA][m+idxA] = fetch(A, m, n, boundA);
    
    #pragma unroll
    for (n = 0; n < BLK_N; n += DIM_YB)
        #pragma unroll
        for (m = 0; m < BLK_K; m += DIM_XB)
            sB[n+idyB][m+idxB] = fetch(B, m, n, boundB);
    
    __syncthreads();

    for (kk = 0; kk < K-BLK_K; kk += BLK_K)
    {
        #ifdef TEXTURE_1D
            coord_A += BLK_K*LDA;
            coord_B += BLK_K;
        #else
            offs_dA += BLK_K*LDA;
            boundA  -= BLK_K*LDA;

            offs_dB += BLK_K;
            boundB  -= BLK_K;
        #endif

        #pragma unroll
        for (n = 0; n < BLK_K/DIM_YA; n++)
            #pragma unroll
            for (m = 0; m < BLK_M/DIM_XA; m++)
                ra[n][m] = fetch(A, m*DIM_XA, n*DIM_YA, boundA);

        #pragma unroll
        for (n = 0; n < BLK_N/DIM_YB; n++)
            #pragma unroll
            for (m = 0; m < BLK_K/DIM_XB; m++)
                rb[n][m] = fetch(B, m*DIM_XB, n*DIM_YB, boundB);
        
        // Multiply
        #pragma unroll
        for (k = 0; k < BLK_K; k++)
        {
            // Load A shmem->regs
            #pragma unroll
            for (m = 0; m < THR_M; m++)
                rA[m] = sA[k][m*DIM_X+idx];

            // Load B shmem->regs
            #pragma unroll
            for (n = 0; n < THR_N; n++)
                rB[n] = sB[n*DIM_Y+idy][k];

            // Compute
            #pragma unroll
            for (n = 0; n < THR_N; n++) {
                #pragma unroll
                for (m = 0; m < THR_M; m++) {
                    fma(op<CONJA>(rA[m]), op<CONJB>(rB[n]), rC[n][m]);
                }
            }
        }

        __syncthreads();

        #pragma unroll
        for (n = 0; n < BLK_K/DIM_YA; n++)
            #pragma unroll
            for (m = 0; m < BLK_M/DIM_XA; m++)
                sA[n*DIM_YA+idyA][m*DIM_XA+idxA] = ra[n][m];
        
        #pragma unroll
        for (n = 0; n < BLK_N/DIM_YB; n++)
            #pragma unroll
            for (m = 0; m < BLK_K/DIM_XB; m++)
                sB[n*DIM_YB+idyB][m*DIM_XB+idxB] = rb[n][m];
        
        __syncthreads();
    }

    // Multiply last full (BLK_K) or partial block of
    // columns of op(A) and rows of op(B).
    // It's okay that m,n exceed matrix bounds as all work is in registers
    // or shared memory, and out-of-bounds rC[n][m] will not be saved later.
    kk = K - kk;
    #pragma unroll
    for (k = 0; k < kk; k++)
    {
        // Load A shmem->regs
        #pragma unroll
        for (m = 0; m < THR_M; m++)
            rA[m] = sA[k][m*DIM_X+idx];

        // Load B shmem->regs
        #pragma unroll
        for (n = 0; n < THR_N; n++)
            rB[n] = sB[n*DIM_Y+idy][k];

        // Compute
        #pragma unroll
        for (n = 0; n < THR_N; n++) {
            #pragma unroll
            for (m = 0; m < THR_M; m++) {
                fma(op<CONJA>(rA[m]), op<CONJB>(rB[n]), rC[n][m]);
            }
        }
    }

    // Store C regs->dev
    if( beta == make_FloatingPoint(0.0,0.0) ) {
        #pragma unroll
        for (n = 0; n < THR_N; n++) {
            int coord_dCn = bly*BLK_N + n*DIM_Y + idy;
            #pragma unroll
            for (m = 0; m < THR_M; m++) {
                int coord_dCm = blx*BLK_M + m*DIM_X + idx;
                if (coord_dCm < M && coord_dCn < N) {
                    int offsC = coord_dCn*LDC + coord_dCm;

                    double &regC = rC[n][m];
                    double &memC = C[offsC];

                    memC = mul(alpha, regC);
                }
            }
        }
    } else {
        #pragma unroll
        for (n = 0; n < THR_N; n++) {
            int coord_dCn = bly*BLK_N + n*DIM_Y + idy;
            #pragma unroll
            for (m = 0; m < THR_M; m++) {
                int coord_dCm = blx*BLK_M + m*DIM_X + idx;
                if (coord_dCm < M && coord_dCn < N) {
                    int offsC = coord_dCn*LDC + coord_dCm;

                    double &regC = rC[n][m];
                    double &memC = C[offsC];

                    memC = add(mul(alpha, regC), mul(beta, memC));
                }
            }
        }
    }
#endif /* (__CUDA_ARCH__ >= 200) */
}

void invoke_kernel(kernel_params * args){
    const double* a = args->a;
    const double* b = args->b;
    double* c = args->c;
	
	double alpha = args->alpha;
	double beta = args->beta;
	
    size_t m = args->m;
    size_t k = args->k;
    size_t n = args->n;

    dim3 dimBlock(DIM_M, DIM_N);
    int dim_grid_m = m%BLK_M == 0 ? m/BLK_M : m/BLK_M+1;
    int dim_grid_n = n%BLK_N == 0 ? n/BLK_N : n/BLK_N+1;
    dim3 dimGrid(dim_grid_m, dim_grid_n);

    // warm up
    gemm_template_device_nn<<<grid, threads, 0, 0>>> (m, k, n, a, b, c, alpha, beta);
    }

class Benchmark{
    std::vector<double> h_a;
    std::vector<double> h_b;
    std::vector<double> h_c;

    double* d_a;
    double* d_b;
    double* d_c;

    size_t m;
    size_t k;
    size_t n;
	
	double alpha{1.2};
	double beta{2.1};

public:
    void make_input(size_t M, size_t K, size_t N){
		std::cout << "make input: " << std::endl;
		std::cout << "    M " << M << std::endl;
		std::cout << "    K " << K << std::endl;
		std::cout << "    N " << N << std::endl;
		
        m = M;
        k = K;
        n = N;

        h_a.resize(m * k, 1.1);
        h_b.resize(k * n, 1.1);
        h_c.resize(m * n, 1.1);
        }

    void copy_to_device(){
		std::cout << "send data to device" << std::endl;
		
        CUDA_CALL_CHECK(cudaMalloc(&d_a, h_a.size() * sizeof(double)));
        CUDA_CALL_CHECK(cudaMalloc(&d_b, h_b.size() * sizeof(double)));
        CUDA_CALL_CHECK(cudaMalloc(&d_c, h_c.size() * sizeof(double)));

        CUDA_CALL_CHECK(cudaMemcpy(d_a, h_a.data(), h_a.size() * sizeof(double),
            cudaMemcpyHostToDevice));

        CUDA_CALL_CHECK(cudaMemcpy(d_b, h_b.data(), h_b.size() * sizeof(double),
            cudaMemcpyHostToDevice));

        CUDA_CALL_CHECK(cudaMemcpy(d_c, h_c.data(), h_c.size() * sizeof(double),
            cudaMemcpyHostToDevice));

        CUDA_CALL_CHECK(cudaDeviceSynchronize());
        CUDA_CALL_CHECK(cudaGetLastError());
        }

    void invoke_kernel(){
		std::cout << "invoke kernel" << std::endl;
		
        kernel_params p;
        p.a = d_a;
        p.b = d_b;
        p.c = d_c;
        p.m = m;
        p.k = k;
        p.n = n;
		p.alpha = alpha;
		p.beta = beta;
       
		// warm up
		std::cout << "invoke warm up code" << std::endl;
		::invoke_kernel(std::addressof(p));
		CUDA_CALL_CHECK(cudaDeviceSynchronize());
		
		std::cout << "invoke target test" << std::endl;
        auto startTP = std::chrono::steady_clock::now();

        const int iterCount = 2;
        for(int iter = 0; iter < iterCount; ++iter){
		std::cout << "  iteration " << iter << std::endl;
            ::invoke_kernel(std::addressof(p));
			CUDA_CALL_CHECK(cudaDeviceSynchronize());
            }

        auto stopTP = std::chrono::steady_clock::now();
        auto mcsecTotal = std::chrono::duration_cast<std::chrono::microseconds>(stopTP - startTP);

        // Compute and print the performance
        float mcsecPerMatrixMul = mcsecTotal.count() / iterCount;

        double flopsPerMatrixMul = 2.0 * static_cast<double>(k) *
            static_cast<double>(m) *
            static_cast<double>(n);
        double tFlops =
            flopsPerMatrixMul / (mcsecPerMatrixMul * 1000000.0f);

        std::cout << "Performance= " << tFlops << ",TFlop" << std::endl;
        }

    void copy_from_device(){
        CUDA_CALL_CHECK(cudaDeviceSynchronize());
        CUDA_CALL_CHECK(cudaGetLastError());
        }

    void free_data()noexcept{
        h_a.clear();
        h_b.clear();
        h_c.clear();

        CUDA_CALL_CHECK(cudaFree(d_a));
        CUDA_CALL_CHECK(cudaFree(d_b));
        CUDA_CALL_CHECK(cudaFree(d_c));
        }
    };

/**
 * Program main
 */
int main(int argc, char** argv){

    Benchmark test;
    test.make_input(8192, 8192, 8192);
    test.copy_to_device();
    test.invoke_kernel();
    test.copy_from_device();
    test.free_data();
}
	




