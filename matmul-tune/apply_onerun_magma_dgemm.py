import subprocess
import time
import math
import sys
import os

warp_size = 32
real_size = 8

# for compute capability 8.0
max_threads_per_block = 1024
max_shmem_per_block = (128*1024)
max_regs_per_thread = 255
max_regs_per_block = 65536

# heuristic constraints
min_threads_per_block = 256
min_fmas_per_load = 64

class TuneError(Exception):
	"""Some build error"""
	pass

def execute(cmd):
    print(f'execute: {cmd}')

    start_counter = time.perf_counter_ns()

    retcode = -1
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    while True:
            output = p.stdout.readline()
            if output :
                print(output.strip())
        
            if p.poll() is not None:
                retcode = p.poll()
                break
    
    duration_ns = time.perf_counter_ns() - start_counter
    
    if retcode != 0:
        print("\nSome error")
        raise TuneError
        
    tflops = (2 * pow(8192, 3))/(duration_ns * 1000)
    return tflops
        
def build_args_libraries(llibs):
    string = '';
    for libname in llibs:
        string = string + (' -l' + libname)
    return string
    
def build_args_sources(sources):
    return ' '.join(sources)

def build_args_configuration(block_size, 
    dim_x, dim_y, 
    blk_m, blk_n, blk_k, 
    dim_xa, dim_ya, dim_xb, dim_yb):
    return 
    ' -DBLOCK_SIZE=' + str(block_size) + 
    ' -DDIM_X=' + str(dim_x) +
    ' -DDIM_Y=' + str(dim_y) +
    ' -DBLK_M=' + str(blk_m) +
    ' -DBLK_N=' + str(blk_n) +
    ' -DBLK_K=' + str(blk_k) +
    ' -DDIM_XA=' + str(blk_xa) +
    ' -DDIM_YA=' + str(blk_ya) +
    ' -DDIM_XB=' + str(dim_xb) +
    ' -DDIM_YB=' + str(dim_yb)

def loop_configuration():
    print('loop over configurations')

    min_tflops = 1000000000
    max_tflops = 0
    
    for block_size in range(8,32+1,8):
        shmem_per_block = block_size*block_size*2
        if shmem_per_block > max_shmem_per_block:
            continue
                
        for dim_x in range(4, 64+1, 4):
          for dim_y in range(4, 64+1, 4):
            threads_per_block = dim_x*dim_y

            if threads_per_block > max_threads_per_block:
              continue

            if threads_per_block % warp_size != 0:
              continue

            if threads_per_block < min_threads_per_block:
              continue

            for blk_m in range(dim_x, 128+1, dim_x):
              for blk_n in range(dim_y, 128+1, dim_y):

                regs_per_thread = (blk_m/dim_x)*(blk_n/dim_y)
                if regs_per_thread > max_regs_per_thread:
                  continue

                regs_per_block = regs_per_thread*threads_per_block
                if regs_per_block > max_regs_per_block:
                  continue

                for dim_xa in range(4, blk_m+1, 4):
                  for dim_ya in range(4, 64+1, 4):

                    if dim_xa*dim_ya != dim_x*dim_y:
                      continue

                    for dim_xb in range(4, 64+1, 4):
                      for dim_yb in range(4, blk_n+1, 4):

                        if dim_xb*dim_yb != dim_x*dim_y:
                          continue

                        for blk_k in range(4, 64+1, 4):

                            if blk_k%dim_ya != 0:
                              continue

                            if blk_k%dim_xb != 0:
                              continue

                            loads_per_block = blk_m*blk_k + blk_k*blk_n
                            shmem_per_block = loads_per_block*float_size
                            if shmem_per_block > max_shmem_per_block:
                              continue

                            fmas_per_block = blk_m*blk_n*blk_k
                            fmas_per_load = fmas_per_block/loads_per_block
                            if fmas_per_load < min_fmas_per_load:
                              continue
          
                            print(f'configure : block size = {block_size}' )
                            print(f'          : dim_x = {dim_x}' )
                            print(f'          : dim_y = {dim_y}' )
                            print(f'          : dim_xa = {dim_xa}' )
                            print(f'          : dim_ya = {dim_ya}' )
                            print(f'          : dim_xb = {dim_xb}' )
                            print(f'          : dim_yb = {dim_yb}' )
                            print(f'          : blk_m = {blk_m}' )
                            print(f'          : blk_k = {blk_k}' )
                            print(f'          : blk_n = {blk_n}' )
                            
                            compiler = 'nvcc'
                            sources_list = ['onerun_magma_dgemm.cu']
                            link_list = ['cudart']

                            execute(compiler + ' -o app_magma_dgemm ' + 
                                build_args_configuration(block_size,
                                block_size, 
                                dim_x, dim_y, 
                                blk_m, blk_n, blk_k, 
                                dim_xa, dim_ya, dim_xb, dim_yb) + 
                                ' '  + build_args_sources(sources_list) + 
                                build_args_libraries(link_list))
                              
                            print(f'execute binary')
                            
                            tflops = execute('./app_magma_dgemm')
                            
                            if min_tflops > tflops:
                                min_tflops = tflops
                                
                            if max_tflops < tflops:
                                max_tflops = tflops
                                
                            print(f'max TFLOPS={max_tflops}, min TFLOPS={min_tflops}')
        
if __name__ == "__main__":
	loop_configuration()